function(doc, req) {  
	registerType("binary", "application/octet-stream");
  return provides("binary", function() {
		var random = "";
		for (var i = 0; i < 5120; i++) {
			random += String.fromCharCode(Math.floor((Math.random()*1000) % 100));
		}
		return random;
	});
}